from django.shortcuts import render,redirect
from django.http import JsonResponse
from wallet.models import Wallet,Transaction
from django.views.generic import CreateView
from django.contrib.auth.decorators import login_required
import re
from decimal import Decimal


@login_required
def withdrow(request):
	user = request.user
	if request.method =='POST':
		value = request.POST.get('amount')
		try:
			wallet = Wallet.objects.get(user=user)
		except:
			wallet = 0
		if int(value)>wallet.current_balance:
			context = {
				"balance":wallet,
				"error":"Insufficient funds"
			}
			return render(request,'wallet/withdraw.html',context)
		else:
			wallet = Wallet.objects.get(user=user)
			balance = wallet.current_balance - int(value)
			Wallet.objects.filter(user=user).update(current_balance=balance)
			context = {
			"balance":wallet
			}
			return redirect('/wallet/withdrow/')#render(request,'wallet/withdraw.html',context)
	else:
		wallet = Wallet.objects.get(user=user)
		context = {
			"balance":wallet
		}
		return render(request,'wallet/withdraw.html',context)


@login_required
def deposit(request):
	user = request.user
	if request.method=='POST':
		try:
			value = request.POST.get('amount')
			wallet = Wallet.objects.get(user=user)
			total_amount =wallet.current_balance+int(value)
			Wallet.objects.filter(user=user).update(current_balance=total_amount)
			context ={
				"balance":wallet
			}
			return redirect('/wallet/deposit/')
		except:
			total_amount = request.POST.get('amount')
			Wallet.objects.filter(user=user).update(current_balance=total_amount)
			context ={
				"balance":wallet
			}
			return redirect('/wallet/deposit/')#render(request,'wallet/deposit.html',context)
	else:
		wallet = Wallet.objects.get(user=user)
		context = {

			"balance":wallet
		}
		return render(request,'wallet/deposit.html',context)


# @login_required
# def transfer(request):
# 	return render(request,'wallet/transfer.html')

# 	