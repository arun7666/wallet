from django.db import models,IntegrityError
from decimal import Decimal
from django.contrib.auth.models import User


class InsufficientBalance(IntegrityError):
	"""Raised when a wallet has insufficient balance to
    run an operation.
    We're subclassing from :mod:`django.db.IntegrityError`
    so that it is automatically rolled-back during django's
    transaction lifecycle.
	"""

class Wallet(models.Model):
    # We should reference to the AUTH_USER_MODEL so that
    # when this module is used and a different User is used,
    # this would still work out of the box.
    #
    # See 'Referencing the User model' [1]
    user = models.OneToOneField(User,on_delete=models.CASCADE)

    # This stores the wallet's current balance. Also acts
    # like a cache to the wallet's balance as well.
    current_balance = models.DecimalField(max_digits=25, default=0, decimal_places=2)
    # The date/time of the creation of this wallet.
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user)

   


class Transaction(models.Model):
    # The wallet that holds this transaction.
    wallet = models.ForeignKey(Wallet,on_delete=models.CASCADE)

    # The value of this transaction.
    value = models.DecimalField(max_digits=6, decimal_places=2)

    # The value of the wallet at the time of this
    # transaction. Useful for displaying transaction
    # history.
    running_balance = models.DecimalField(max_digits=6, decimal_places=2)

    # The date/time of the creation of this transaction.
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
    	return str(self.wallet)