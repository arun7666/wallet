from django.shortcuts import render,redirect
from django.views.generic import CreateView,TemplateView
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login as auth_login
from wallet.models import Wallet,Transaction


class HomePage(TemplateView):
	template_name = "base.html"
	def get_context_data(self,*args,**kwargs):
		context = super(HomePage,self).get_context_data(**kwargs)
		context['current_balance'] =  Wallet.objects.all()
		return context

def signup(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/')
	else:
		form = UserCreationForm()
		return render(request,"accounts/signup.html",{"form":form})


def login(request):
	if request.method=='POST':
	    username = request.POST['username']
	    password = request.POST['password']
	    user = authenticate(request, username=username, password=password)
	    if user is not None:
	        auth_login(request, user)
	        return redirect('/')
	    else:
	    	return render(request,'accounts/login.html')
	else:
		return render(request,'accounts/login.html')
