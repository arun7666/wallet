from .base import *

DEBUG = config('DEBUG',cast=bool)

ALLOWED_HOSTS = ['production-ip','domain-name']


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
